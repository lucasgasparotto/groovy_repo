pipeline {
    agent any 
    parameters {
        string(name:'FOLDER', defaultValue: '"test"', description: 'Folder to be sent to GDrive/Movies')
        string(name: 'DESTINATION', defaultValue: '"Destination"', description: 'Destination folder')
    }
    stages {
        stage ('Run shell script') {
            steps {
                sh "/home/lgasparotto/Downloads/movies.sh ${params.FOLDER} ${params.DESTINATION}"
            }
        }
        
    }
}