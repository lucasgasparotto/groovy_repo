pipeline {
    agent any 
    parameters {
        string(name:'FOLDER', defaultValue: 'test', description: 'Folder to be sent to GDrive/Movies')
    }
    stages {
        stage ('Create destination folder') {
            steps {
                sh 'mkdir /home/lgasparotto/GDrive/Movies/${params.FOLDER}'
            }
        }
        stage ('Begin copy') {
            steps {
                sh 'rclone copy --no-traverse -p /home/lgasparotto/Downloads/${params.FOLDER} GDrive:Movies/${params.FOLDER}'
            }
        }
        stage ('Checking') {
            steps {
                sh 'if [ -d "/home/lgasparotto/GDrive/Movies/${params.FOLDER}" ]; then rm -Rf /home/lgasparotto/Downloads/${params.FOLDER}; fi'
            }
        }
    }
}